// console.log("Hello")


fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then((json) => console.log(json.map(function(json){
        let title = {'title': json.title}
        return title;
    })));		



// get
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'GET'
})
.then((response) => response.json())
.then((json) => console.log(json, `The item "${json.title}" on the list has a status of ${json.completed}`))


// post
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		"completed": false,
		"userId": 1,
    	"title": "Created to do List Item",
    	"body": "update Post File"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));



//update
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		
		"title": "Updated to do List Item",
		"description": "To update my to do list with a different data structure.",
		"status": "pending",
		"dateCompleted": "pending",
    	"userId": 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));



//patch
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		"status": "completed",
		"dateCompleted": "01/31/23"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));



//delete
fetch('https://jsonplaceholder.typicode.com/todos/1', {
method: 'DELETE'
});

